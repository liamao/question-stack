const availableSites = require("../sites.js");

module.exports = {
    name: 'list',
    description: 'List all available StackExchange sites to query.',
    usage: 'list',
    cooldown: 5,
    execute(client, message, args) {
        const data = [];
        const keys = Object.keys(availableSites.sites);

        data.push('Here\'s a list of all available StackExchange sites..');
        data.push(keys.map(key => key).join(', '));
        data.push(`\nUse \`?ask [site] [question]\` to query a specific site!`);
        return message.channel.send(data, { split: true });
    },
};