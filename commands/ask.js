const axios = require('axios');
const availableSites = require("../sites.js");

module.exports = {
    name: 'ask',
    description: 'Query StackExchange with a question.',
    usage: 'ask [site] [question]',
    cooldown: 30,
    async execute(client, message, args) {
        const data = [];
        const answerReturn = [];
        const sites = Object.keys(availableSites.sites);

        if (!args.length) {
            data.push('Incorrect command usage..');
            data.push('Please use command `?help` to see proper command usage');
            message.channel.send(data, { split: true });
            return;
        }
        const siteArg = args[0].toLowerCase();
        let site = "stackoverflow";

        if (sites.includes(siteArg)){
            site = availableSites.sites[siteArg];
            args.shift();
        } 

        const question = args.join(' ');
        message.channel.send(`Gathering answers from **${site}**.....`);
        data.push("Select one of the following solution titles:")
        await axios.get(`https://api.stackexchange.com/2.2/search/advanced?order=desc&sort=activity&q=${question}&accepted=True&answers=1&site=${site}`)
          .then(response => {
            let answers = response.data.items;
            if (answers.length > 5) {
              answers = answers.slice(0, 5);
            }
            answers.forEach((answer, index) => {
                data.push((index + 1) + ".) "  + answer.title);
                answerReturn.push(answer);
            });
            message.reply(data);
          })
          .catch(error => {
            console.log(error);
          });
        return answerReturn;
    },
};