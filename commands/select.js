const Discord = require('discord.js');

module.exports = {
    name: 'select',
    description: 'Select a specific answer from your search response',
    usage: 'select [number]',
    cooldown: 5,
    execute(client, message, args, items) {
        const data = [];

        if (args.length !== 1) {
            data.push('Incorrect command usage..');
            data.push('Please use command `?help` to see proper command usage');
            message.channel.send(data, { split: true });
            return;
        }

        let selectNum = parseInt(args[0]);
        if (isNaN(selectNum)) { 
            message.channel.send("Your selection must be a number 1-5!"); 
            return false;
        }

        if (selectNum > items.length || selectNum <= 0){
            message.channel.send("Selected answer does not exist... try again.")
        }

        const select = items[selectNum - 1];
        message.reply(createEmbed(select));
        return true;
    },
};

function createEmbed(answer){
    const answerResponse = new Discord.MessageEmbed()
    .setColor('#0099ff')
    .setTitle(answer.title)
    .setURL(answer.link)
    .setAuthor(answer.owner.display_name, answer.owner.profile_image)
    .setThumbnail('https://cdn.sstatic.net/Sites/stackoverflow/company/Img/logos/so/so-icon.svg?v=f13ebeedfa9e')
    .setTimestamp()
    return answerResponse;
}
