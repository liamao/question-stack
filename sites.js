exports.sites = {
	stackoverflow: 'stackoverflow',
	unix: 'unix' ,
	aqrade: 'gaming',
	math: 'math',
	homeimprovement: 'diy',
	anime: 'anime',
	softwareengineering: 'softwareengineering',
	graphicdesign: 'graphicdesign',
	computerscience: 'cs',
	biology: 'biology',
	history: 'history',
	writing: 'writing',
	sports: 'sports',
	politics: 'politics',
	coffee: 'coffee'
}
