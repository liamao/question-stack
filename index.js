const Discord = require('discord.js');
const client = new Discord.Client();
const fs = require('fs');
const token = require('./token.json');
const colors = require('colors');

client.commands = new Discord.Collection();
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
let userSelect = {}

for (const file of commandFiles) {
    const command = require(`./commands/${file}`);
    client.commands.set(command.name, command);
}

client.on('ready', () => {
    console.log('QuestionStack Active!'.green);
    console.log(`Currently active in ${client.guilds.cache.size} servers`.green)
});

client.on('message', async message => {
    if  (message.content.charAt(0) !== '?') return;
    const args = message.content.slice(1).split(/ +/);
    if (new RegExp("^[?\?]+$").test(args[0])) return;
    if (args[0] === "admin") return admin(client, message, args);
    const command = client.commands.get(args[0]) ||
                    client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(args[0]));
	
    if (!command) {
      return message.channel.send("Command not found! Use `?help` for a list of available commands.");
    }

    args.shift();
    try {
      if (command.name === 'select'){
        if (userSelect[client]){
          if (command.execute(client, message, args, userSelect[client])){
              delete userSelect[client];
              return;
          }
        }
        return message.reply("You have no answers to select! To ask a question, use `?ask`");
      } else if (command.name === 'ask'){
        const responses = await command.execute(client, message, args);
        if (responses && responses.length > 1){
          userSelect[client] = responses;
        }
      } else {
        command.execute(client, message, args);
      }
    } catch (error) {
      console.error(error);
      message.reply('Error executing command! Please report at https://gitlab.com/liamao/question-stack/-/issues!');
    }

});

function admin(client, message, args){
  if (message.channel.guild.id === "712109814984999006"){
    args.shift();
    if (args[0] === "count"){
      const count = client.guilds.cache.size;
      message.reply("QuestionStack is currently in " + count + " servers.");
    } else if (args[0] === "api"){
    } 
  }
}


client.login(token.token);
